import { Router } from "express";
import { contactController } from "../controllers/index.js";
import { sortFilterPagination } from "../middleware/sortSelectPage.js";
export const contactRouter = Router();

contactRouter
  .route("/")
  .post(contactController.createContact)
  .get(contactController.readAllContact, sortFilterPagination);
contactRouter
  .route("/:id")
  .patch(contactController.updateContact)
  .get(contactController.readSpecificContact)
  .delete(contactController.deleteSpecificContact);

export default contactRouter;
