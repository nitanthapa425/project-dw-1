import { Router } from "express";
// import addressRouter from "./addressRouter.js";
import addRouter from "./addRouter.js";
// import addressRouter from "./addressRouter.js";
import adminRegisterRouter from "./adminRegisterRouter.js";
import blogRouter from "./blogRouter.js";
import contactRouter from "./contactRouter.js";
import fileUploadRouter from "./fileUpload.js";
import productRouter from "./productRouter.js";
import reviewRouter from "./reviewSchema.js";
// import reviewRouter from "./reviewSchema.js";
// import reviewRouter from "./reviewSchema.js";

const apiRouter = Router();

const ourRoutes = [
  {
    path: `/admin`,
    router: adminRegisterRouter,
  },
  {
    path: `/file`,
    router: fileUploadRouter,
  },
  {
    path: `/products`,
    router: productRouter,
  },
  {
    path: `/reviews`,
    router: reviewRouter,
  },
  {
    path: `/blogs`,
    router: blogRouter,
  },
  {
    path: `/contacts`,
    router: contactRouter,
  },
  {
    path: `/address`,
    router: addRouter,
  },
];

ourRoutes.forEach((route) => {
  apiRouter.use(route.path, route.router);
});

export default apiRouter;
