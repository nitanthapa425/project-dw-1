import { Router } from "express";
import { blogController } from "../controllers/index.js";
import { sortFilterPagination } from "../middleware/sortSelectPage.js";
export const blogRouter = Router();

blogRouter
  .route("/")
  .post(blogController.createBlog)
  .get(blogController.readAllBlog, sortFilterPagination);
blogRouter
  .route("/:id")
  .patch(blogController.updateBlog)
  .get(blogController.readSpecificBlog)
  .delete(blogController.deleteSpecificBlog);

export default blogRouter;
