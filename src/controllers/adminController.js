import { expiryIn, secretKey } from "../config/config.js";
import { HttpStatus } from "../constant/constant.js";
import successResponseData from "../helper/successResponseData.js";
import tryCatchWrapper from "../middleware/tryCatchWrapper.js";

import { adminService } from "../services/index.js";
import { comparePassword, hashPassword } from "../utils/hashFunction.js";
import { generateToken } from "../utils/token.js";

// register
//login
//logout
//  my profile
//update register (profile)//we do not update email and password
//update password for login user (delete token after update password and don't change password if password are same)
//forgetpassword(reset password)
//delete

export let createAdminUser = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  let passHashedPassword = await hashPassword(body.password);
  body.password = passHashedPassword;
  let data = await adminService.createAdminUserService({ body });
  successResponseData({
    res,
    message: "Admin Created Successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});

export let loginAdminUser = tryCatchWrapper(async (req, res) => {
  let email = req.body.email;
  let password = req.body.password;

  // let data = await loginAdminUserService(req.body.email, req.body.password);
  //if email exist
  //if password match
  //token send
  let user = await adminService.readSpecificAdminUserByAny({ email });
  if (user === null) {
    let error = new Error("Please enter valid email or password.");
    error.statusCode = HttpStatus.UNAUTHORIZED;
    throw error;
  } else {
    let isValidPassword = await comparePassword(password, user.password);
    if (isValidPassword) {
      let infoObj = { userId: user._id, role: user.role };

      let token = await generateToken(infoObj, secretKey, expiryIn);

      let data = {
        token: token,
      };
      // await TokenData.create(data);
      await adminService.createTokenService({ data });
      console.log(token);

      successResponseData({
        res,
        message: "Login Successfully.",
        statusCode: HttpStatus.OK,
        data: token,
      });
    } else {
      let error = new Error("Please enter valid email or password.");
      error.statusCode = HttpStatus.UNAUTHORIZED;
      throw error;
    }
  }
});

export let logoutAdminUser = tryCatchWrapper(async (req, res) => {
  let id = req.token.tokenId;
  await adminService.deleteSpecificTokenService({ id });

  successResponseData({
    res,
    message: "Logout Successfully.",
    statusCode: HttpStatus.OK,
  });
  // instead of 204 , 200 is better because we pass some data
});

export let updateAdminUser = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  delete body.password;
  delete body.email;
  let id = req.info.userId;
  let data = await adminService.updateSpecificAdminUserService({ id, body });

  successResponseData({
    res,
    message: "User updated successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});

export let updateAdminPassword = tryCatchWrapper(async (req, res) => {
  //don't allow to updated if previous and present password are same(it is said to be good practice)
  let id = req.info.userId;
  let password = req.body.password;
  // let data = await updateAdminPasswordService(req.info.userId);

  let user = await adminService.readSpecificAdminUserService({ id });
  let isPreviousCurrentPasswordSame = await comparePassword(
    password,
    user.password
  );
  if (isPreviousCurrentPasswordSame) {
    let error = new Error("Previous and Current password are same.");
    error.statusCode = 400;
    throw error;
  }
  let body = {
    password: await hashPassword(password),
  };
  // let result = await Admin.findByIdAndUpdate(userId, data, { new: true });
  let data = await adminService.updateSpecificAdminUserService({ id, body });
  delete data._doc.password;
  //removing token after update
  await adminService.deleteSpecificTokenService({ id: req.token.tokenId });
  // return result;

  successResponseData({
    res,
    message: "User password updated successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});

export let adminMyProfile = tryCatchWrapper(async (req, res) => {
  let id = req.info.userId;
  let data = await adminService.readSpecificAdminUserService({ id });
  // instead of console result
  //doe let r1={...result}
  //then it will show exact result
  //instead of deleting like
  // delete result.password;
  // do like this
  // delete result._doc.password;
  successResponseData({
    res,
    message: "Profile read successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

// export let deleteAdminUser = tryCatchWrapper(async (req, res) => {
//   let id = req.params.id;
//   // let userId = req.info.userId;
//   let data = await adminService.deleteSpecificAdminUserService({ id });
//   delete data?._doc?.password;
//   successResponseData({
//     res,
//     message: "Delete profile successfully.",
//     statusCode: HttpStatus.OK,
//     data,
//   });
// });

export let readAllAdminUser = tryCatchWrapper(async (req, res, next) => {
  let find = {};

  if (req.query.email) {
    find.email = { $regex: req.query.email, $options: "i" };
  }

  if (req.query.firstName) {
    find.firstName = req.query.firstName;
  }

  req.find = find;
  req.service = adminService.readAllAdminService;
  req.myOwnSelect = "-password";
  next();
});

export let readSpecificAdminUser = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;
  let data = await adminService.readSpecificAdminUserService({ id });
  delete data._doc.password;
  successResponseData({
    res,
    message: "Read user successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

export let deleteSpecificAdminUser = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;

  let data = await adminService.deleteSpecificAdminUserService({ id });
  delete data?._doc?.password;
  successResponseData({
    res,
    message: "Admin deleted successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

export let updateSpecificAdminUser = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;
  let body = { ...req.body };

  delete body.password;
  delete body.email;

  let data = await adminService.updateSpecificAdminUserService({ id, body });

  successResponseData({
    res,
    message: "Admin User updated successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});
