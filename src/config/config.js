import { config } from "dotenv";
config();
export const dbUrl =
  process.env.DB_URL || "mongodb://localhost:27017/project-dw-1";
export const port = process.env.PORT;

export const staticFolder = "./public";
export const apiVersion = process.env.API_VERSION || "/api/v1";
export const secretKey = process.env.SECRET_KEY || "project1";
export const expiryIn = process.env.EXPIRY_IN || "365d";
export const baseUrl = process.env.BASE_URL || "localhost:8000"; // baseUrl is facebook.com is used for image
