import { Product } from "../schemasModle/model.js";

export const createProductService = async ({ data }) => Product.create(data);
export const updateProductService = async ({ id, data }) =>
  Product.findByIdAndUpdate(id, data, {
    new: true,
    runValidators: true,
  });
export const readSpecificProductService = async ({ id }) =>
  Product.findById(id);
export const readAllProductService = async ({
  find = {},
  sort = "",
  limit = "",
  skip = "",
  select = "",
}) => Product.find(find).sort(sort).limit(limit).skip(skip).select(select);
export const deleteSpecificProductService = async ({ id }) =>
  Product.findByIdAndDelete(id);
