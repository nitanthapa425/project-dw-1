import { model } from "mongoose";
import { addressSchema } from "./schemas/addressSchema.js";
import adminRegisterSchema from "./schemas/adminRegister.js";
import { blogSchema } from "./schemas/blogSchema.js";
import { contactSchema } from "./schemas/contactSchema.js";
import productSchema from "./schemas/productSchema.js";
import { reviewSchema } from "./schemas/reviewSchema.js";
import { tokenSchema } from "./schemas/tokenSchema.js";

export const Admin = model("Admin", adminRegisterSchema);
export const TokenData = model("TokenData", tokenSchema);
export const Product = model("Product", productSchema);
export const Review = model("Review", reviewSchema);
export const Address = model("Address", addressSchema);
export const Blog = model("Blog", blogSchema);
export const Contact = model("Contact", contactSchema);
