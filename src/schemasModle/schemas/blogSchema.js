import { Schema } from "mongoose";

export let blogSchema = Schema(
  {
    title: {
      type: String,
      trim: true,
      required: [true, "title field is required"],
    },
    description: {
      type: String,
      trim: true,
      required: [true, "description field is required"],
    },
    img: {
      type: String,
      trim: true,
      required: [true, "img field is required"],
    },
  },

  {
    timestamps: true,
  }
);
